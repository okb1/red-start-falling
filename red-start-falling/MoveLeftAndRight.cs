using Godot;
using System;

public class MoveLeftAndRight : Node2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private float time;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		time += delta ;

		//var t = Transform;
		//t.origin.x = Mathf.Sin(time);
		//Transform = t;
		;

		Position = new Vector2(GetViewport().GetVisibleRect().Size.x / 2 + Mathf.Sin(time) * 100, GetViewport().GetVisibleRect().Size.y/2);
	}
}
