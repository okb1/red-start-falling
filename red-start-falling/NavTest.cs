using Godot;
using System;

public class NavTest : Navigation2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Vector2[] path;
	private Sprite sprite;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		path = this.GetSimplePath(Position, Position + new Vector2(100, 50), false);


		sprite = GetNode<Sprite>(GetPath() + "/Sprite");
		if(sprite == null)
			GD.Print(Name, " Cannot find sprite child names 'Sprite'");
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		path = this.GetSimplePath(Position, Position + new Vector2(100, 50), false);
		Update();
	}

	public override void _Draw()
	{
		GD.Print(path.Length);
		if (path.Length <= 1)
			return;

		for (var i = 1; i < path.Length; i++)
		{
			var pointA = path[i-1];
			var pointB = path[i];
			DrawLine(ToLocal(pointA), ToLocal(pointB), Color.Color8(0, 255, 0));
		}
	}
}
