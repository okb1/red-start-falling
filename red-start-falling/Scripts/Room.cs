using Godot;
using Godot.Collections;

[Tool]
public class Room : Area2D
{
	[Export]
	public Array<NodePath> neighbourRooms;

	private float oxygen;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public override void _Process(float delta)
	{
		Update();
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Draw()
	{
		if (Engine.EditorHint)
		{
			foreach (var roomPath in neighbourRooms)
			{
				var room = GetNode<Node2D>(roomPath);
				if (room != null)
				{
					DrawLine(Vector2.Zero, room.GetGlobalTransform().origin - GetGlobalTransform().origin + new Vector2(1.2f, 1.2f), Color.Color8((byte)Position.x, 0, 0));
				}
			}
		}
	}
}
