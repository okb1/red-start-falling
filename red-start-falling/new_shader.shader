shader_type canvas_item;
render_mode skip_vertex_transform;

void vertex() {
	VERTEX = (EXTRA_MATRIX * (WORLD_MATRIX * vec4(VERTEX, 0.0, 1.0f))).xy;
}
