﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleAudioSetup : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        AudioSource sfx = GetComponent<AudioSource>();
        float genpitch = Random.RandomRange(.8f, 1.2f);
        sfx.pitch = genpitch;
        sfx.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
