﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBarManager : MonoBehaviour
{

    public int characterIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CharacterInstance[] characters = GameObject.FindObjectsOfType<CharacterInstance>();

        gameObject.transform.Find("HeathBar").GetComponent<RectTransform>().transform.localScale = new Vector2(characters[characterIndex].Heath , 1.0f);
        gameObject.transform.Find("OxygenBar").GetComponent<RectTransform>().transform.localScale = new Vector2(characters[characterIndex].Oxygen, 1.0f);
        gameObject.transform.Find("RadiationBar").GetComponent<RectTransform>().transform.localScale = new Vector2(characters[characterIndex].Radiation, 1.0f);
    }
}
