﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSystem : MonoBehaviour
{
    [Tooltip("Time in seconds before the asteroids start spawning")]
    public float startDelay = 5f;
    [Tooltip("Time between asteroids")]
    public float downtime = 20f;
    [Tooltip("Amount of downtime decreased per spawned asteroid")]
    public float downtimeRampdown = 1f;

    // Unused atm...
    public GameObject asteroidPrefab;
    public GameObject[] hullHolePrefabs;
    public GameObject[] firePrefabs;

    [SerializeField] RoomInstance nextRoom;
    public RoomInstance[] rooms;

    public float nextAsteroidTime = 0.0f;
    public float nextFireExplosionTime = 0.0f;

    public void Start()
    {
        rooms = GameObject.FindObjectsOfType<RoomInstance>();

        nextAsteroidTime = Time.time + Random.Range(4.0f, 8.0f);
        nextFireExplosionTime = Time.time + Random.Range(startDelay, 10.0f);
    }
    public void Update()
    {
        if (Time.time > nextAsteroidTime)
        {
            //pick a new time for the next astroid to spawn
            //NOTE(max): We should probably base these values based on how many
            //           asteroids there already are.
            nextAsteroidTime = Time.time + Random.Range(12.0f, 24.0f);


            //spawn a hole in a random room
            SpawnHoleInRandomRoom();

        }

        if(Time.time > nextFireExplosionTime)
        {
            nextFireExplosionTime = Time.time + Random.Range(4.0f, 10.0f);

            SpawnFireInRandomRoom();
        }
    }

    public bool SpawnHoleInRandomRoom()
    {
        //Gather all the rooms without holes
        int numberOfRoomsWithoutHoles = 0;
        RoomInstance[] roomsWithoutHoles = new RoomInstance[rooms.Length];
        for (int i = 0; i < rooms.Length; ++i)
        {
            if (rooms[i].holes.Count == 0)
            {
                roomsWithoutHoles[numberOfRoomsWithoutHoles++] = rooms[i];
            }
        }

        //If we have no rooms with space to place a hole, we will return false
        if (numberOfRoomsWithoutHoles == 0)
        {
            return false; //failed to spawn a hole
        }

        //pick a random room from the list of rooms without a hole
        int roomThatsFuckedIdx = Random.Range(0, numberOfRoomsWithoutHoles);
        RoomInstance roomThatsFucked = roomsWithoutHoles[roomThatsFuckedIdx];


        //create a new hole object
        GameObject hole = Instantiate(randomPrefabFromList(hullHolePrefabs));
        Bounds holeBounds = hole.GetComponent<SpriteRenderer>().bounds;

        //calculate the center position of the room so we can place the hole there.
        Bounds roomBounds = roomThatsFucked.GetComponent<Collider2D>().bounds;
        Vector3 roomCenter = roomBounds.center;

        //Place the hole in the center of the room.
        hole.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 2 * Mathf.PI));
        hole.transform.position = new Vector3(
            roomCenter.x,
            roomCenter.y,
            -1.0f);/* + new Vector3(
            holeBounds.size.x * .5f, -holeBounds.size.y * .5f, -10.0f);*/

        //add the hole to the list of holes in that room
        //NOTE(max): I am assuming we are only going to be having one hole per room right now.
        roomThatsFucked.holes.Add(hole);

        return true; //successfully spawn a hole in aroom
    }

    public void SpawnFireInRandomRoom()
    {
        RoomInstance room = getRoomToBeBurned();
        if (!room)
            return;

        GameObject fire = Instantiate(randomPrefabFromList(firePrefabs));
        Bounds roomBounds = room.GetComponent<Collider2D>().bounds;
        Vector3 roomCenter = roomBounds.center;
        roomCenter.z = -5.0f;
        //fire.transform.parent = room.transform;
        fire.transform.position = roomCenter;

        room.AddFire(fire);
    }

    private RoomInstance getRoomToBeBurned()
    {
        List<RoomInstance> roomWithoutFire = new List<RoomInstance>(rooms.Length);
        foreach (var room in rooms)
        {
            if (room.fires.Count == 0)
                roomWithoutFire.Add(room);
        }
        if (roomWithoutFire.Count == 0)
            return null;

        int i = Random.Range(0, roomWithoutFire.Count);
        return roomWithoutFire[i];
    }

#if false
    IEnumerator Start()
    {
        rooms = GameObject.FindObjectsOfType<RoomInstance>();

        nextRoom = getRoomToBeBroken();

        yield return new WaitForSeconds(startDelay);

        StartCoroutine(SpawnNextAsteroid());
    }

    IEnumerator SpawnNextAsteroid()
    {
        //var asteroid = Instantiate(asteroidPrefab, Vector3.zero, Quaternion.identity);

        var hole = Instantiate(randomPrefabFromList(hullHolePrefabs));
        var holeBounds = hole.GetComponent<SpriteRenderer>().bounds;

        var roomBounds = nextRoom.GetComponent<Collider2D>();
        var spawnVolume = roomBounds.bounds;
        spawnVolume.min += holeBounds.size * 0.5f;
        spawnVolume.max -= holeBounds.size * 0.5f;

        hole.transform.parent = nextRoom.transform;
        var pointInRoom = randomPointInBounds(ref spawnVolume);
        pointInRoom.z -= 5f;
        hole.transform.position = pointInRoom;

        nextRoom.AddHole(hole);

        nextRoom = getRoomToBeBroken();
        yield return new WaitForSeconds(downtime);

        downtime -= downtimeRampdown;

        StartCoroutine(SpawnNextAsteroid());
    }
    private RoomInstance getRoomToBeBroken()
    {
        int i = Random.Range(0, rooms.Length);
        return rooms[i];
    }
    private Vector3 randomPointInBounds(ref Bounds bounds)
    {
        float x = Random.Range(bounds.min.x, bounds.max.x);
        float y = Random.Range(bounds.min.y, bounds.max.y);
        float z = Random.Range(bounds.min.z, bounds.max.z);

        return new Vector3(x, y, z);
    }
#endif

    private GameObject randomPrefabFromList(GameObject[] prefabs)
    {
        int i = Random.Range(0, prefabs.Length);
        return prefabs[i];
    }
}
