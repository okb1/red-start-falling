﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class CharacterHolder : MonoBehaviour
{
    public GameObject[] cosmonauts;
    int character_index;
    public Color Selected_Color = Color.blue;
    public Color UnSelected_Color = Color.red;
    public class EventInt : UnityEvent<int> { };
    public EventInt cosmonaut_switched = new EventInt();
    [HideInInspector]
    public GameObject current_cosmonaut;
    [SerializeField] private float timeLeftToWin = 300; //60 is equal to one minute
    [HideInInspector]
    public string timer;

    private bool charactersAlive = false;

    // Start is called before the first frame update
    void Start()
    {

        character_index = 0;
        current_cosmonaut = cosmonauts[0];
        Set_Current_Cosmonaut(character_index, true);

        for (int i = 0; i < cosmonauts.Length; i++)
        {
            cosmonauts[i].GetComponent<CharacterInstance>().index = i;
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();

        if (Input.GetButtonDown("SwitchCharacterRight"))
        {
            Set_Current_Cosmonaut(character_index, false);
            if (character_index >= cosmonauts.Length - 1)
            {
                character_index = 0;
                Set_Current_Cosmonaut(character_index);
            }
            else
            {
                character_index++;
                Set_Current_Cosmonaut(character_index);
            };

        }
        if (Input.GetButtonDown("SwitchCharacterLeft"))
        {
            Set_Current_Cosmonaut(character_index, false);
            if (character_index <= 0)
            {
                character_index = cosmonauts.Length - 1;
                Set_Current_Cosmonaut(character_index, true);

            }
            else
            {
                character_index--;
                Set_Current_Cosmonaut(character_index, true);
            }
            
        }

        if (Input.GetMouseButtonDown(0))
        {
            var filter = new ContactFilter2D();
            filter.useTriggers = true;

            List<RaycastHit2D> hits = new List<RaycastHit2D>(10);
            int amountOfHitsOrSomething = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, filter, hits);

            foreach (RaycastHit2D hit in hits)
            {
                if (hit.transform)
                {
                    var movementCmp = hit.transform.GetComponent<ClickToMove>();
                    if (movementCmp)
                    {
                        character_index = GetCosmonautIndex(movementCmp.gameObject);
                        var movementCmps = GameObject.FindObjectsOfType<ClickToMove>();
                        foreach (var movement in movementCmps)
                        {
                            Set_Current_Cosmonaut(GetCosmonautIndex(movement.gameObject), false);
                        }

                        Set_Current_Cosmonaut(character_index, true);
                    }
                }
            }
        }
    }

    private int GetCosmonautIndex(GameObject cosmonaut)
    {
        for(int i = 0; i < cosmonauts.Length; ++i)
        {
            if (cosmonauts[i] == cosmonaut)
                return i;
        }

        return -1;
    }

    private void Set_Current_Cosmonaut(int cosmonaut_index,bool selected = true)
    {
        current_cosmonaut = cosmonauts[cosmonaut_index];
        current_cosmonaut.GetComponent<ClickToMove>().IsSelected = selected;
        if (selected)
        {
            current_cosmonaut.GetComponentInChildren<SpriteRenderer>().color = Selected_Color;

        }
        else
        {
            current_cosmonaut.GetComponentInChildren<SpriteRenderer>().color = UnSelected_Color;

        }
        cosmonaut_switched.Invoke(character_index);
    }


    void UpdateTimer()
    {
        if (timeLeftToWin > 0)
        {
            timeLeftToWin -= Time.deltaTime;
            int minutes = Mathf.FloorToInt(timeLeftToWin / 60f);
            int seconds = Mathf.RoundToInt(timeLeftToWin % 60f);
            timer = minutes + "." + seconds;

            charactersAlive = false;
            for (int i = 0; i < cosmonauts.Length; ++i)
            {
                if (cosmonauts[i].GetComponent<CharacterInstance>().isAlive)
                    charactersAlive = true;
             }

            if (!charactersAlive)
                SceneManager.LoadScene("LoseScreen");
        }
        else if(timeLeftToWin <= 0)
        {
            SceneManager.LoadScene("WinScreen");
        }
    }
}

