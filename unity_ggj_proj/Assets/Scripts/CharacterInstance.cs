﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.LowLevel;

public class CharacterInstance : MonoBehaviour
{
    private float radiationLevel = 0;
    private float heathLevel = 0;
    private float oxygenLevel = 1;

    public bool isRepairing = false;
    public bool isAlive = true;

    [HideInInspector] public float Radiation { get { return radiationLevel; } }
    [HideInInspector] public float Heath { get { return heathLevel; } }
    [HideInInspector] public float Oxygen { get { return oxygenLevel; } }

    [SerializeField] private float oxygenFlowSpeed = 0.2f;
    [SerializeField] private float maxOxygen = 1;

    [SerializeField] private Animator cosmonautAnimator;
    private Pathfinding.IAstarAI astarAgent;

    [HideInInspector] public bool needsOxygen = false;
    [HideInInspector] public bool tooMuchRadiation = false;
    [HideInInspector] public bool tooMuchHeath = false;

    [HideInInspector] public int index = 0;
    /*
    public void DecereaseOxygen(float amount)
    {
        oxygenLevel = Mathf.Max(0, oxygenLevel - amount * oxygenFlowSpeed);

        // trigger effect?
    }

    public void IncreaseOxygen(float amount)
    {
        oxygenLevel = Mathf.Min(maxOxygen, oxygenLevel + amount * oxygenFlowSpeed);

        // trigger effect?
    }
    */



    void Start()
    {
        astarAgent = GetComponent<Pathfinding.IAstarAI>();

        oxygenLevel = maxOxygen;


    }

    void Update()
    {
        cosmonautAnimator.SetFloat("Oxygen", oxygenLevel);
        cosmonautAnimator.SetFloat("Velocity", astarAgent.velocity.magnitude);
        cosmonautAnimator.SetBool("IsAlive", isAlive);
        cosmonautAnimator.SetBool("IsRepairing", isRepairing);

        //find what room the cosmonaut is currently residing in
        Collider2D[] colliders = new Collider2D[128]; //128 should be enough, right
        int hitCount = Physics2D.GetContacts(GetComponent<Collider2D>(), colliders);
        if (hitCount > 128)
        {
            Debug.LogWarning("Went over imposed limit of 128 colliders we could hit");
        }
        hitCount = Mathf.Min(hitCount, 128); //clamp just in case

        //find all the rooms in the list of object we collided with
        int roomHitCount = 0;
        RoomInstance[] roomsWeCollideWith = new RoomInstance[128];
        for (int i = 0; i < hitCount; ++i)
        {
            RoomInstance r = colliders[i].GetComponent<RoomInstance>();
            if (r != null) roomsWeCollideWith[roomHitCount++] = r;
        }

        //find the average oxygen of the rooms we collide with
        float roomOxygenLevelAverage = 0f;
        if (roomHitCount > 0)
        {
            float roomOxygenLevelAccum = 0.0f;
            for (int i = 0; i < roomHitCount; ++i)
            {
                roomOxygenLevelAccum += roomsWeCollideWith[i].oxygenLevel;
            }
            roomOxygenLevelAverage = roomOxygenLevelAccum / (float)roomHitCount;
        }

        //
        // Update the character's oxygen level based on the room's oxygen level
        //
        float oxygenDeathSpeed;
        //determine a base value
        oxygenDeathSpeed = Mathf.Clamp(roomOxygenLevelAverage - 0.5f, -0.5f, 0.5f) * 2.0f;
        //TODO(max): apply a slight curve
        //apply a constant scalar to the death speed
        oxygenDeathSpeed = oxygenDeathSpeed * (1.0f / 12.0f); //10 seconds in worst case

        //apply the chance to the oxygen level
        oxygenLevel += oxygenDeathSpeed * Time.deltaTime;
        if (oxygenLevel <= 0.0f)
        {
            //mark as dead
            isAlive = false;
        }
        //clamp the oxygen level between 0 and 1
        oxygenLevel = Mathf.Clamp(oxygenLevel, 0.0f, 1.0f);


        //remove oxygen from the rooms
        float oxygenToRemovePerRoom = 0.1f * Time.deltaTime / (float)roomHitCount;
        for (int i = 0; i < roomHitCount; ++i)
        {
            roomsWeCollideWith[i].oxygenLevel = Mathf.Max(
                0.0f,
                roomsWeCollideWith[i].oxygenLevel - oxygenToRemovePerRoom);
        }

        //Heal heat
        heathLevel -= RoomManager.Singleton.FireHealingSpeed * Time.deltaTime;
        heathLevel = Mathf.Clamp01(heathLevel);

        //Add heat
        for (int i = 0; i < roomHitCount; ++i)
        {
            var room = roomsWeCollideWith[i];
            float heatTransfer = room.fires.Count * RoomManager.Singleton.FireSpreadingSpeed * Time.deltaTime;
            heathLevel += heatTransfer;
        }
        heathLevel = Mathf.Clamp01(heathLevel);

        if(heathLevel >= 1.0)
        {
            isAlive = false;
        }

        Debug.Log("Repairing: " + isRepairing);


        // Flip character when moving in the opposite direction
        if (Mathf.Abs(astarAgent.velocity.x) > 0.5f)
        {
            var cosmonautScale = cosmonautAnimator.transform.localScale;
            cosmonautScale.x = Mathf.Abs(cosmonautScale.x) * Mathf.Sign(astarAgent.velocity.x);
            cosmonautAnimator.transform.localScale = cosmonautScale;
        }


        if (isRepairing)
        {
            //TODO: Set animator and add proper FX
            Debug.DrawLine(transform.position, transform.position + new Vector3(Mathf.Sin(Time.time), Mathf.Cos(Time.time)));
        }

        if (oxygenLevel < 0.2f)
        {
            needsOxygen = true;
        }
        else
            needsOxygen = false;

        if (heathLevel > 0.8f)
        {
            tooMuchHeath = true;
        }
        else
            tooMuchHeath = false;

        if (radiationLevel > 0.8f)
        {
            tooMuchRadiation = true;
        }
        else
            tooMuchRadiation = false;

        
    }
}

