﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToMove : MonoBehaviour
{
    public bool IsSelected = false;
    private NavMeshAgent agent;
    private Pathfinding.IAstarAI astarAgent;
    private CharacterInstance character;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        astarAgent = GetComponent<Pathfinding.IAstarAI>();
        character = GetComponent<CharacterInstance>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsSelected && character.isAlive)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                const float maxHit = 1000f;
                
                if (Physics.Raycast(ray, out hit, int.MaxValue, 0, QueryTriggerInteraction.Collide))
                {
                    if(agent != null)
                        agent.destination = hit.point;
                }

                if (astarAgent != null)
                    astarAgent.destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
    }
}
