﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInstance : MonoBehaviour
{
    [SerializeField] bool _isOpen = true;
    public bool IsOpen => _isOpen;
    [SerializeField] private Sprite[] openClose;
    public RoomInstance roomFrom;
    public RoomInstance roomTo;

    SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        spriteRenderer.sprite = _isOpen ? openClose[0] : openClose[1];
        Debug.Log(spriteRenderer);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ContactFilter2D filter = new ContactFilter2D();
            filter.useTriggers = true;
            List<RaycastHit2D> results = new List<RaycastHit2D>();
            Physics2D.Raycast(
                Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector2.zero,
                filter,
                results);

            foreach (var hit in results)
            {
                if (hit.transform == this.transform)
                {
                    _isOpen = !_isOpen;
                    spriteRenderer.sprite = _isOpen ? openClose[0] : openClose[1];

                }
            }
        }
    }

    public RoomInstance GetOtherRoom(RoomInstance entryRoom)
    {
        if (entryRoom == roomFrom)
            return roomTo;
        if (entryRoom == roomTo)
            return roomFrom;

        throw new Exception("This room does not attach to this door at all!");
    }
}
