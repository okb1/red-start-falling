﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour
{
    public float timerToMenu;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timerToMenu > 100.0f)
        {
            timerToMenu = 0;
            SceneManager.LoadScene("Menu");
        }
        timerToMenu += 0.5f;
    }
}
