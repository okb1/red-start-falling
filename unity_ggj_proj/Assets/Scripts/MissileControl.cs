﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MissileControl : MonoBehaviour
{
    public string missileLaunchTextPrefix = "Missiles launching in...";
    public TextMeshPro textMesh;
    public SpriteRenderer controlPanelGraphic;
    public float timeAvailableForRepair = 30.0f;
    private RoomInstance room;
    public AudioSource BreakdownAudio;
    public AudioSource AlarmAudio;

    private float brokenFrom;
    private bool wasBrokenAlready = false;

    // Start is called before the first frame update
    void Start()
    {
        room = GetComponent<RoomInstance>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (room.IsRoomBroken())
        {
            //Start timer
            if (!wasBrokenAlready)
            {
                brokenFrom = Time.time;
                wasBrokenAlready = true;

                textMesh.text = missileLaunchTextPrefix + "30";
                textMesh.gameObject.SetActive(true);
                // handle audio components
                BreakdownAudio.Play();
                float breakdownLength = BreakdownAudio.clip.length;
                AlarmAudio.PlayDelayed(breakdownLength);



                controlPanelGraphic.material.color = Color.red;
            }
        }

        if(wasBrokenAlready)
        {
            if (!room.IsRoomBroken())
            {
                textMesh.gameObject.SetActive(false);
                controlPanelGraphic.material.color = Color.white;

                wasBrokenAlready = false;
                // Stop audio
                BreakdownAudio.Stop();
                AlarmAudio.Stop();
            }

            textMesh.text = missileLaunchTextPrefix + (int)(timeAvailableForRepair - (Time.time - brokenFrom));


            if (brokenFrom + timeAvailableForRepair < Time.time)
            {
                //TODO: Add FX
                Debug.Log("Game over the missiles are away!!");
                controlPanelGraphic.material.color = Color.black;
                Time.timeScale = 0f;
            }
        }
    }
}
