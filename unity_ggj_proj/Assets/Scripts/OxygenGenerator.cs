﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RoomInstance))]
public class OxygenGenerator : MonoBehaviour
{
    [Tooltip("Per second")]
    public float oxygenGeneration = 0.4f;
    private RoomInstance room;

    public bool isBroken = false;
    public float nextBreakDownTime;

    // Start is called before the first frame update
    void Start()
    {
        room = GetComponent<RoomInstance>();
        nextBreakDownTime = Time.time + Random.Range(20.0f, 40.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextBreakDownTime)
        {
            nextBreakDownTime = Time.time + Random.Range(20.0f, 40.0f);


        }


#if false
        if (!room.IsRoomBroken())
        {
            room.oxygenLevel += oxygenGeneration * Time.deltaTime;
            room.oxygenLevel = Mathf.Clamp01(room.oxygenLevel);
        }
#endif
    }
}
