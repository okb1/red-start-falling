﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PanCameraWithMouse : MonoBehaviour
{
    public float marginToBorder = 10.0f;
    public float movementSpeed = 5.0f;
    private Camera camera;

    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        var mousePos = Input.mousePosition;
        var xAbs = Math.Abs( mousePos.x );
        var yAbs = Math.Abs( mousePos.y );

        var cameraPan = Vector2.zero;
        if (xAbs <= marginToBorder)
        {
            cameraPan.x -= 1f;
        }
        if(xAbs > Screen.width - marginToBorder)
        {
            cameraPan.x += 1f;
        }
        if(yAbs < marginToBorder)
        {
            cameraPan.y -= 1f;
        }
        if(yAbs > Screen.height - marginToBorder)
        {
            cameraPan.y += 1f;
        }

        transform.Translate(cameraPan * movementSpeed * Time.deltaTime);
    }
}
