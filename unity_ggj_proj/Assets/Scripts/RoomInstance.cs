﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomInstance : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    [SerializeField] public TextMesh _text;
    public TextMesh Text => _text;
    [SerializeField] public RoomInstance[] _roots;
    [SerializeField] public DoorInstance[] _doors;
    public RoomInstance[] Roots => _roots;
    public DoorInstance[] Doors => _doors;

    [HideInInspector] public RoomManager manager;
    public void SetManager(RoomManager v) { manager = v;}

    [SerializeField] public float hullBreachFlowSpeed = 1.0f;
    [SerializeField] public float startOxygenLevel = 1;
    public List<CharacterInstance> _characters = new List<CharacterInstance>();
    public DoorInstance[] attachedDoors;
	public float oxygenLevel = 1;

    public List<GameObject> holes = new List<GameObject>();
    public List<GameObject> fires = new List<GameObject>();
    public float holeFixingProgress = 0;
    public float fireFixingProgress = 0;

    public void Init()
    {
        oxygenLevel = 0.5f;
        _text.text = oxygenLevel.ToString();
        _doors = GetComponentsInChildren<DoorInstance>();
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    private void UpdateColor()
    {
        float v = Remap(1-oxygenLevel,0.5f,1f, 0, 1);
        float r = 64 / 256.0f;
        float g = 211 / 256.0f;
        float b = 255 / 256.0f;
        float a = 134 / 256.0f;
            _spriteRenderer.color = (new Color(r, g, b, a * v));
    }

    public bool IsRoomBroken()
    {
        return holes.Count > 0;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.isTrigger)
            return;

        CharacterInstance instance = collider.GetComponent<CharacterInstance>();
        if (instance != null) _characters.Add(instance);
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.isTrigger)
            return;

        CharacterInstance instance = collider.GetComponent<CharacterInstance>();
        if (instance != null)
        {
            instance.isRepairing = false;
            _characters.Remove(instance);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < _characters.Count; ++i)
        {
            CharacterInstance charInstance = _characters[i];
            //charInstance.DecereaseOxygen(1.0f);
            if (holes.Count > 0 || fires.Count > 0)
            {
                charInstance.isRepairing = true;
            }
            else
            {
                charInstance.isRepairing = false;
            }
        }

        if (holes.Count > 0 || fires.Count > 0)
        {
            if (holes.Count > 0)
            {
                holeFixingProgress += _characters.Count * Time.deltaTime;

                if (holeFixingProgress >= manager.HoleFixingTime)
                {
                    holeFixingProgress = 0f;
                    FixHole();
                }
            }
            else
            {
                if (fires.Count > 0)
                {
                    fireFixingProgress += _characters.Count * Time.deltaTime;

                    if(fireFixingProgress >= manager.FireFixingTime)
                    {
                        fireFixingProgress = 0f;

                        FixFire();
                    }
                }
            }

        }
        else
        {
            foreach(var character in _characters)
            {
                character.isRepairing = false;
            }
        }


        // Drain oxygen when when hull is broken
        var loss = hullBreachFlowSpeed * holes.Count * Time.deltaTime;
        oxygenLevel -= loss;
        oxygenLevel = Mathf.Max(oxygenLevel, 0f);
        UpdateColor();
    }


    // child should be updated first!
    public void UpdateOxygenFlow(RoomInstance flowFrom)
    {
        float max = manager.OxygenFlowSpeed * Time.deltaTime;
        float diff = (flowFrom.oxygenLevel - oxygenLevel) * .5f;
        if (Math.Abs(diff) < Mathf.Epsilon) return;
        if (Mathf.Abs(diff) > max)
        {
            float v = max / Mathf.Abs(diff);
            diff *= v;
        }
        oxygenLevel += diff;
        flowFrom.oxygenLevel -= diff;
    }

    public void AddHole(GameObject hole)
    {
        holes.Add(hole);
    }

    public void FixHole()
    {
        if (holes.Count == 0)
            return;

        int holeIndex = UnityEngine.Random.Range(0, holes.Count);
        Destroy(holes[holeIndex]);
        holes.RemoveAt(holeIndex);
    }
    public void AddFire(GameObject fire)
    {
        fires.Add(fire);
    }

    public void FixFire()
    {
        if (fires.Count == 0)
            return;

        int fireIndex = UnityEngine.Random.Range(0, fires.Count);
        Destroy(fires[fireIndex]);
        fires.RemoveAt(fireIndex);
    }
}
