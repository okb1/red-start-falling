﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    class RoomData { public RoomInstance room; public DoorInstance door;}

    [SerializeField] private float oxygenFlowSpeed = 1;
    [SerializeField] private float oxygenAddSpeed = .015f;
    [SerializeField] private float holeFixingTime = 5f;
    [SerializeField] private float fireFixingTime = 5f;
    [SerializeField] private float fireSpreadingSpeed = 0.1f;
    [SerializeField] private float fireHealingSpeed = 0.005f;

    public float OxygenFlowSpeed => oxygenFlowSpeed;
    public float OxygenAddSpeed => oxygenAddSpeed;
    public float HoleFixingTime => holeFixingTime;
    public float FireFixingTime => fireFixingTime;
    public float FireSpreadingSpeed => fireSpreadingSpeed;
    public float FireHealingSpeed => fireHealingSpeed;
    private bool _paused = false;
    private RoomInstance[] _rooms;
    private RoomInstance[] _roots;
    Dictionary<RoomInstance, RoomData[]> _roomChildren = new Dictionary<RoomInstance, RoomData[]>();

    //I need this
    public static RoomManager Singleton = null;

    private void Awake()
    {
        Singleton = this;
    }

    bool IsChildOf(RoomInstance child, RoomInstance parent)
    {
        for (int i = 0; i < child.Roots.Length; ++i)
            if (child.Roots[i] == parent)
                return true;

        return false;
    }

    RoomData[] GetChildren(RoomInstance root, RoomInstance[] rooms)
    {
        List<RoomData> children = new List<RoomData>();
        for (int j = 0; j < _rooms.Length; ++j)
        {
            if (IsChildOf(_rooms[j], root))
            {
                DoorInstance door = null;
                    if(_rooms[j].Doors.Length > 0)
                        door = (_rooms[j].Doors.Length == 1 ? _rooms[j].Doors[0]
                    : GetClosestDoor(root.transform.position, _rooms[j].Doors[0], _rooms[j].Doors[1]));

                RoomData data = new RoomData();
                data.room = _rooms[j];
                data.door = door;

                if(door == null) Debug.Log(rooms[j] + " doesnt have a door");
                children.Add(data);
            }
        }

        return children.ToArray();
    }

    DoorInstance GetClosestDoor(Vector3 roomOrigin, DoorInstance a, DoorInstance b)
    {
        float distA = Vector3.Distance(roomOrigin, a.transform.position);
        float distB = Vector3.Distance(roomOrigin, b.transform.position);
        return distA < distB ? a : b;
    }

    RoomInstance[] GetNextRoots(RoomInstance currentRoot, RoomInstance[] rooms)
    {
        List<RoomInstance> roots = new List<RoomInstance>();
        for (int i = 0; i < rooms.Length; ++i)
        {
            RoomInstance room = rooms[i];
            for (int j = 0; j < room.Roots.Length; ++j)
            {
                if (room.Roots[j] == currentRoot)
                    roots.Add(room);
            }
        }

        return roots.ToArray();
    }

    void Start()
    {
        Singleton = this;


        _rooms = FindObjectsOfType<RoomInstance>();
        RoomInstance root = null;

        Dictionary<RoomInstance, RoomInstance[]> roomRootsDictionary = new Dictionary<RoomInstance, RoomInstance[]>();
        for (int i = 0; i < _rooms.Length; ++i)
        {
            roomRootsDictionary.Add(_rooms[i], _rooms[i].Roots);
            _rooms[i].SetManager(this);
            _rooms[i].Init();

            // find the root
            RoomInstance instance = _rooms[i];
            if (instance.Roots.Length == 0)
                root = instance;
        }

        if (root == null) Debug.LogError("ROOT NOT FOUND! (oxygen supply)");

        RoomData rootData = new RoomData();
        List<RoomInstance> currentRoots = new List<RoomInstance> { root };
        List<RoomInstance> nextRoots = new List<RoomInstance> { };
        List<RoomInstance> roots = new List<RoomInstance>();
        while (currentRoots.Count > 0)
        {
            for (int i = 0; i < currentRoots.Count; ++i)
            {
                RoomData[] children = GetChildren(currentRoots[i], _rooms);
                if (children.Length > 0)
                {
                    roots.Add(currentRoots[i]);
                    _roomChildren.Add(currentRoots[i], children);
                    nextRoots.AddRange(GetNextRoots(currentRoots[i], _rooms));
                }
            }
            currentRoots.Clear();
            currentRoots.AddRange(nextRoots);
            nextRoots.Clear();
        }

        _roots = new RoomInstance[roots.Count];
        Array a = roots.ToArray();
        Array.Copy(a,_roots,a.Length);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_paused)
        {
            AddOxygen();
            FlowOxygen();
            ClampOxygen();
            UpdateTextElements();
        }
    }

    private void ClampOxygen()
    {
        for (int i = 0; i < _rooms.Length; ++i)
        {
            _rooms[i].oxygenLevel = Mathf.Clamp01(_rooms[i].oxygenLevel);
        }
    }

    private void FlowOxygen()
    {
        //for (int i = 0; i < _roots.Length; ++i)
        //{
        //    RoomData[] children = _roomChildren[_roots[i]];
        //    for (int j = 0; j < children.Length; ++j)
        //    {
        //        if(children[j].door != null && children[j].door.IsOpen)
        //            children[j].room.UpdateOxygenFlow(_roots[i]);
        //    }
        //}
        for(int i = 0; i < _rooms.Length; ++i)
        {
            foreach(var door in _rooms[i].attachedDoors)
            {
                if(door && door.IsOpen)
                {
                    var otherRoom = door.GetOtherRoom(_rooms[i]);
                    otherRoom.UpdateOxygenFlow(_rooms[i]);
                }
            }
        }
    }

    private void AddOxygen()
    {
        for (int i = 0; i < _rooms.Length; ++i)
        {
            _rooms[i].oxygenLevel = Mathf.Min(1.0f, _rooms[i].oxygenLevel + oxygenAddSpeed * Time.deltaTime);
        }
    }

    private void UpdateTextElements()
    {
        for (int i = 0; i < _rooms.Length; ++i)
        {
            _rooms[i].Text.text = Math.Round(_rooms[i].oxygenLevel,2).ToString();
        }
    }

}
