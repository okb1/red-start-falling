﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UIManager : MonoBehaviour
{
    public GameObject holderRef;
    private GameObject[] characterUIObjects;

    private GameObject characterPointer;
    

    // Start is called before the first frame update
    void Start()
    {
        characterUIObjects = new GameObject[holderRef.GetComponent<CharacterHolder>().cosmonauts.Length];
        characterPointer = new GameObject();
        characterPointer.AddComponent<SpriteRenderer>();
        characterPointer.GetComponent<SpriteRenderer>().sprite = Resources.Load("UIElements/SelectArrow", typeof(Sprite)) as Sprite;
        characterPointer.transform.localScale = new Vector2(0.1f, 0.1f);
        // characterPointer.transform.Rotate(new Vector3(0, 0, 1), 90);

        int characterUIOffset = 200;
        for(int i = 0; i < characterUIObjects.Length; i++)
        {
            characterUIObjects[i] = Resources.Load("UIElements/CharacterUI") as GameObject;
            characterUIObjects[i] = Instantiate(characterUIObjects[i], new Vector3(50, -127, 0), Quaternion.identity);
            characterUIObjects[i].GetComponentInChildren<UIBarManager>().characterIndex = i;

            characterUIObjects[i].transform.parent = transform.parent;
            characterUIObjects[i].GetComponent<RectTransform>().localPosition = new Vector2((Screen.width /2) - characterUIObjects[i].transform.localScale.x *  120, Screen.height / 2 - characterUIOffset);
       
            characterUIOffset += 300;
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] characters = holderRef.GetComponent<CharacterHolder>().cosmonauts;
        GameObject currCharacter = holderRef.GetComponent<CharacterHolder>().current_cosmonaut;

        for (int i = 0; i < characterUIObjects.Length; i++)
        {
            if(characters[i].GetComponent<CharacterInstance>().needsOxygen)
                FindObjectOfType<UIWarningManager>().TriggerWarning("OxygenMarker", Camera.main.ScreenToWorldPoint(characterUIObjects[i].transform.position - new Vector3(50, 0, -20)));

            if (characters[i].GetComponent<CharacterInstance>().tooMuchHeath)
                FindObjectOfType<UIWarningManager>().TriggerWarning("HeathMarker", Camera.main.ScreenToWorldPoint(characterUIObjects[i].transform.position - new Vector3(50, 0, -20)));

            if (characters[i].GetComponent<CharacterInstance>().tooMuchRadiation)
                FindObjectOfType<UIWarningManager>().TriggerWarning("RadiationMarker", Camera.main.ScreenToWorldPoint(characterUIObjects[i].transform.position - new Vector3(50, 0, -20)));
        }

        characterPointer.transform.position = Camera.main.ScreenToWorldPoint(characterUIObjects[currCharacter.GetComponent<CharacterInstance>().index].transform.position);
        characterPointer.transform.position = new Vector2(characterPointer.transform.position.x - 1, characterPointer.transform.position.y- 0.5f);
    }
    
}
