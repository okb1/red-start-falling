﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class UIOncharacterChange : MonoBehaviour
{
    public GameObject holderRef;
    private GameObject characterPointer;
    [SerializeField]private float arrowSpeed = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        //Creates the arrow bellow the selected character
        characterPointer = new GameObject();
        characterPointer.AddComponent<SpriteRenderer>();
        characterPointer.GetComponent<SpriteRenderer>().sprite = Resources.Load("UIElements/SelectArrow", typeof(Sprite)) as Sprite;
        characterPointer.transform.localScale = new Vector2(0.1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        //Get the current player character
            GameObject current_cos = holderRef.GetComponent<CharacterHolder>().current_cosmonaut;

        //Calculate the new Y for the character pointer
            float newY = Mathf.Sin(Time.time * arrowSpeed);

        //Translate the character pointer to the right posiiton
            characterPointer.transform.position = new Vector3(current_cos.transform.position.x,
                                                              current_cos.transform.position.y + newY - 1,
                                                              -2);
        

       
    }

    void ChangeText(int cosmonaut_int)
    {
        Debug.Log(cosmonaut_int);
        GetComponent<Text>().text = cosmonaut_int.ToString();
        
    }
    
   
}
