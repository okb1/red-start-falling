﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWarningManager : MonoBehaviour
{

    Dictionary<string, GameObject> warning_List = new Dictionary<string, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        AddWarning("OxygenMarker");
        AddWarning("RadiationMarker");
        AddWarning("HeathMarker");


        print(warning_List.Keys);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Adds the warnings to the list with warnings
    public void AddWarning(string warning_Type)
    {
        //Creates a new game object
        GameObject warning_Sprite_Renderer = new GameObject();
        warning_Sprite_Renderer.AddComponent<SpriteRenderer>();

        warning_Sprite_Renderer.GetComponent<SpriteRenderer>().sprite = Resources.Load("UIElements/" + warning_Type, typeof(Sprite)) as Sprite;

        warning_Sprite_Renderer.transform.position = new Vector2(10000, 10000);

        warning_List.Add(warning_Type, warning_Sprite_Renderer);
    }

    public int TriggerWarning(string warning_Type, Vector2 warning_Location)
    {            
            
            Vector2 tempVec = warning_List[warning_Type].transform.localScale;
            tempVec.x += Mathf.Sin(Time.time * 1.5f);
            tempVec.y += Mathf.Sin(Time.time * 1.5f);

            warning_List[warning_Type].transform.localScale = tempVec/5;
            warning_List[warning_Type].transform.position = warning_Location;

     

        return 1;
    }
}
